# El Baratón

El presente proyecto fue realizado con:
* NodeJS v8.11.3
* [Angular CLI](https://github.com/angular/angular-cli) v6.2.0
* Typescript v2.9.2

Probado con Google Chrome 71.0.3578.98.

## Instalación y ejecución
1. Ejecute el comando `npm install` para agregar las dependencias correspondientes.
2. Ejecute `npm start` para compilar e iniciar el proyecto.
3. Una vez compilado con éxito, abra `http://localhost:4200/` para abrir la aplicación (en entorno de desarrollo).