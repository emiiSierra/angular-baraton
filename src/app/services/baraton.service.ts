import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Category } from '../entities/category';
import { Product } from '../entities/product';

@Injectable({
  providedIn: 'root'
})
export class BaratonService {

  constructor(
    private _http: HttpClient
  ) { }


  getCategories(): Observable<Category[]> {
    return this._http.get('./assets/categories.json').pipe(
      map(response =>
        response['categories'].map(cat => new Category(cat))
      )
    );
  }

  getProducts(): Observable<Product[]> {
    return this._http.get('./assets/products.json').pipe(
      map(response =>
        response['products'].map(prod => {
          prod['price'] = parseFloat(prod['price'].replace("$", "").split(",").join("")); 
          return new Product(prod);
        })
      )
    );
  }



}
