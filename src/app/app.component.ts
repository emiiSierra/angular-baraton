import { Component, OnInit } from '@angular/core';
import { Cart } from './entities/cart';
import { Category } from './entities/category';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit  {
  title = 'El Baratón';
  cart: Cart;
  category: Category;
  
  constructor() {
    this.cart = new Cart(localStorage.getItem('cart'));
  }

  ngOnInit() {
  }

  add_to_cart(item) {
    this.cart.add(item);
  }

  set_category(cat) {
    this.category = cat;
  }
}
