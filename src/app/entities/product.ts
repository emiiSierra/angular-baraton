import { AppEntity } from "./appentity";
import { Price } from "./price";

export class Product extends AppEntity {
        
    quantity: number;
    price: Price;
    available: Boolean;
    sublevel_id: number;
    name: string;
    id: string;

    constructor(obj) {
        super(obj);
        this.initTyped(obj, 'price', Price);
    }
}