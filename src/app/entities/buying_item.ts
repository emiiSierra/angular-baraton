import { Product } from "./product";
import { Price } from "./price";

export class BuyingItem {
    product: Product;
    quantity: number;

    constructor(product: Product, q: number) {
        this.product = product;
        this.quantity = q;
    }

    available_to_add(): Boolean {
        return this.product.available && this.quantity <= this.product.quantity;
    }

    available_to_remove(): Boolean {
        return this.product.available && this.quantity > 1;
    }

    available_to_buy(): Boolean {
        return this.product.available;
    }

    add(n) {
        this.quantity += n;
    }

    remove(n) {
        this.quantity -= n;
    }
    
    subtotal(): Price {
        return new Price(this.quantity * this.product.price.value);
    }
    
}