export class Price {
    value: number;
    constructor(n: number | {value: number}) {
        if (typeof(n) === 'number') {
            this.value = n;
        } else {
            this.value = n.value;
        }
    }
    formatted = () => `$ ${new Intl.NumberFormat('es', { maximumFractionDigits: 2 }).format(this.value)}`;
}