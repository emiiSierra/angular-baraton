import { AppEntity } from "./appentity";


export class Category extends AppEntity {
    id: number;
    name: string;
    level = 0;
    sublevels: Category[];

    constructor(obj) {
        super(obj);
        this.initTypedArray(obj, 'sublevels', Category);
        this.sublevels.forEach(subcat => subcat.add_level());
    }

    add_level() {
        this.level++;
        this.sublevels.forEach(sublevel => sublevel.add_level());
    }
    hasSublevels(): boolean {
        return this.sublevels.length > 0;
    }
}
