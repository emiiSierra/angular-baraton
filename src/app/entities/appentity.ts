export class AppEntity {
    constructor(obj) {
        Object.assign(this, obj);
    }

    initTypedArray(input, field, type: typeof AppEntity) {
        if (input[field]) {
            this[field] = input[field].map(el => new type(el));
        } else {
            this[field] = [];
        }
    }

    initTyped(input, field, type: typeof AppEntity | any) {
        if (input[field]) {
            this[field] = new type(input[field]);
        }
    }
}