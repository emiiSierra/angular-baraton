import { BuyingItem } from "./buying_item";
import { Product } from "./product";
import { Price } from "./price";

export class Cart {
    items: BuyingItem[];

    constructor(str?) {
        if (str) {
            const obj = JSON.parse(str);
            this.items = obj.items.map(item => {
                const product = new Product(item.product);
                return new BuyingItem(product, item.quantity);
            });
        } else {
            this.items = [];
        }
        console.log(this);   
    }

    total(): Price {
        return new Price(this.items.reduce((acc, curr) => acc + curr.subtotal().value, 0));
    }

    add(item: BuyingItem) {
        this.items.push(item);
        localStorage.setItem('cart', JSON.stringify(this));
    }

    remove(item: BuyingItem) {
        const index = this.items.indexOf(item);
        this.items.splice(index, 1);
        localStorage.setItem('cart', JSON.stringify(this));
    }

    hasProduct(product: Product): Boolean  {
        return this.items.map(item => item.product).includes(product);
    }

    update() {
        localStorage.setItem('cart', JSON.stringify(this));
    }

}