import { Component, OnInit, Input } from '@angular/core';
import { Cart } from 'src/app/entities/cart';
import { BuyingItem } from 'src/app/entities/buying_item';

@Component({
  selector: 'app-cashout',
  templateUrl: './cashout.component.html',
  styleUrls: ['./cashout.component.css']
})
export class CashoutComponent implements OnInit {
  @Input() cart: Cart;
  
  ngOnInit() {

  }

  add_one_in(item: BuyingItem) {
    item.add(1);
    this.cart.update();
  }

  remove_one_in(item: BuyingItem) {
    item.remove(1);
    this.cart.update();
  }

}
