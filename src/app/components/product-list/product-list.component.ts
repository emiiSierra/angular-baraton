import { Component, OnInit, Input, } from '@angular/core';
import { Product } from 'src/app/entities/product';
import { BaratonService } from 'src/app/services/baraton.service';
import { Cart } from 'src/app/entities/cart';
import { Category } from 'src/app/entities/category';



@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  products: Product[] = [];
  filter = {
    onlyAvailable: false,
    name: '',
    minimum_quantity: 0,
    min_price: 0,
    max_price: 99999
  };
  sort = {
    options: [
      { value: 'quantity', label: 'Cantidad', comparer: (p1: Product, p2: Product) => (p1.quantity > p2.quantity)},
      { value: 'price', label: 'Precio', comparer: (p1: Product, p2: Product) => (p1.price.value > p2.price.value)},
      { value: 'available', label: 'Disponibilidad', comparer: (p1: Product, p2: Product) => (p1.available > p2.available)}
    ],
    current: 'price',
    order: 1
  };

  @Input() cart: Cart;
  @Input() category: Category;


  constructor(
    private _srv: BaratonService
  ) { }

  ngOnInit() {
    this._srv.getProducts().subscribe(response => this.products = response);
  }


  shownProducts(): Product[] {
    const that = this;
    const comparer = this.sort.options.find(op => op.value === that.sort.current).comparer;
    const sorter = (p1: Product, p2: Product) => ((comparer(p1, p2) ? 1 : -1) * that.sort.order);
    return this.products.filter(product => 
      (!that.cart.hasProduct(product)) &&
      (product.sublevel_id === that.category.id) &&
      (product.name.includes(that.filter.name)) &&
      (product.available || !that.filter.onlyAvailable) && 
      (product.quantity >= that.filter.minimum_quantity) &&
      (product.price.value >= that.filter.min_price) &&
      (product.price.value <= that.filter.max_price)
    ).sort(sorter);
  }

  revert_sort() {
    this.sort.order = this.sort.order * -1;
  }

  add_to_cart(item) {
    this.cart.add(item);
  }
}
