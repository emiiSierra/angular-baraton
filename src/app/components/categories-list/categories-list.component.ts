import { Component, OnInit, Injectable, Output, EventEmitter } from '@angular/core';
import { Category } from 'src/app/entities/category';
import { BaratonService } from 'src/app/services/baraton.service';
import {FlatTreeControl} from '@angular/cdk/tree';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import {BehaviorSubject, Observable, of as observableOf} from 'rxjs';

@Injectable()
export class CategoryInject {
  dataChange = new BehaviorSubject<Category[]>([]);

  get data(): Category[] { return this.dataChange.value; }

  constructor(
    private _srv: BaratonService
  ) {
    this._srv.getCategories().subscribe(response => {
      const data = response;
      this.dataChange.next(data);
    });
  }

}

export class CategoryFlatNode {
  constructor(
    public id: number,
    public name: string,
    public expandable: boolean,
    public level: number,
    public sublevels: number
  ) {

  }
}



@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.css'],
  providers: [CategoryInject]
})
export class CategoriesListComponent implements OnInit {
  treeControl: FlatTreeControl<CategoryFlatNode>;
  treeFlattener: MatTreeFlattener<Category, CategoryFlatNode>;
  dataSource: MatTreeFlatDataSource<Category, CategoryFlatNode>;

  @Output() set_category = new EventEmitter<Category>();


  constructor(database: CategoryInject) {
    this.treeFlattener = new MatTreeFlattener(
      (cat: Category) => new CategoryFlatNode(cat.id, cat.name, cat.hasSublevels(), cat.level, cat.sublevels.length),
      this._getLevel,
      this._isExpandable,
      (cat: Category): Observable<Category[]> => observableOf(cat.sublevels)
    );
    this.treeControl = new FlatTreeControl<CategoryFlatNode>(this._getLevel, this._isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

    database.dataChange.subscribe(data => this.dataSource.data = data);

  }

  private _getLevel = (cat: CategoryFlatNode) => cat.level;
  private _isExpandable = (cat: CategoryFlatNode) => cat.expandable;

  ngOnInit() {
  }

  hasSublevels = (_: number, cat: CategoryFlatNode) => cat.expandable;

  changeCurrentCategory(category) {
    this.set_category.emit(category);
  } 
}
