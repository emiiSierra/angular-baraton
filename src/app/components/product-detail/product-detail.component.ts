import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Product } from 'src/app/entities/product';
import { BuyingItem } from 'src/app/entities/buying_item';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  @Input() product: Product;
  @Output() buy = new EventEmitter<BuyingItem>();
  item_to_buy: BuyingItem;

  constructor() { }

  ngOnInit() {
    this.item_to_buy = new BuyingItem(this.product, 1); 
  }

  add_to_cart() {
    this.buy.emit(this.item_to_buy);
  }

}
